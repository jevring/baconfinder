package baconfinder.bacon;

import baconfinder.path.Link;
import baconfinder.path.Path;
import baconfinder.db.Database;
import baconfinder.db.SingleConnectionDataSource;
import baconfinder.path.PathFormatter;
import baconfinder.spf.NodeLookupException;
import baconfinder.spf.NodeProvider;
import baconfinder.spf.ShortestPathFinder;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

/**
 * This class finds the actor or actress with the highest bacon number.
 * It does this by staring with Kevin Bacon and fanning out into every
 * actor/actress that has worked with him. In step n+1, it does the same
 * this for all actors in step n. This naive implementation will generate
 * a path, but it may not be the shortest. To ensure that a path is indeed
 * the shortest, we will keep track of and check a set of conditions along
 * the way. These conditions should also prevent us from creating loops.
 * <p/>
 * In a way, it is similar to the depth-first-search model checker from before.
 * The primary difference is that we want the path of states, and those paths
 * have to be the shortest paths.
 * <p/>
 * Creator: captain
 * Created: 2011-04-10-12:41
 */
public class BaconFinder {
    private final Database db;

    public BaconFinder(Database db) {
        this.db = db;
    }
/*
    @Deprecated
    private void findLongestPath(Integer actorId, Path path) throws SQLException {
        for (Integer title : db.getTitlesForActor(actorId)) {
            for (Integer actor : db.getActorsForTitle(title)) {
                if (!path.contains(actor)) {
                    // if it already exists, then it is, by definition, a shorter path, since all edge costs are the same (1).
                    // This is ONLY because we do a depth-first search.
                    // this is basically an act of pruning branches in the tree.

                    // todo: this doesn't work. We get huge paths with every cast member of each movie
                    // what we need to do is make an actual tree and then prune branches when we find existing nodes.
                    // we do this to avoid loops.
                    // ahh, but loops might be useful!
                    // they might help us find a shorter path between two people.
                    // clearly we have to think this through more.

                    // The naive way of doing it is to generate the tree out from bacon, and then, once
                    // we've found all the leaves, do SPF between bacon and the leaves.
                    // that seems like a lot of extra work, though, when there's probably a way to do it
                    // so that we can get the result in the first path over





                    String actorName = db.getActorName(actor);
                    String titleName = db.getTitleName(title);
                    path.addLink(actor, title, actorName, titleName);
                    System.out.println("Added link to path " + path);
                } else {
                    System.out.println("Path " + path + " already contains actor " + actor);
                }
            }
        }
    }
*/
    public Path findLongestPath(String actor) throws SQLException, NodeLookupException {
        Integer actorId = db.getActorId(actor);
        if (actorId == null) {
            System.out.println("Could not find actor: " + actor);
            // couldn't find that actor
            return null;
        }
        ShortestPathFinder spf = new ShortestPathFinder(actorId, new DatabaseNodeProvider(db));
        Map<Integer,Path> shortestPaths = spf.findShortestPaths();
        // with as many paths as there are actors, almost, we can't print this
        // it would be nice to get the top, say, 10 or 25, though
        //System.out.println("Paths: " + shortestPaths);

        List<Path> longPaths = new LinkedList<Path>();
        Path longestPath = null;
        for (Path path : shortestPaths.values()) {
            if (longestPath == null || longestPath.length() < path.length()) {
                longestPath = path;
            }
            if (path.length() > 7) {
                longPaths.add(path);
            }
        }
        System.out.println("Long paths (" + longPaths.size() + "): ");
        for (Path longPath : longPaths) {
            System.out.println(longPath);
        }

        return longestPath;
    }

    public Map<Integer, Path> findLongestPaths(Integer node) throws NodeLookupException {
        ShortestPathFinder spf = new ShortestPathFinder(node, new NodeProvider<Link>() {
            @Override
            public List<Link> getConnections(Integer node) throws NodeLookupException {
                switch (node) {
                    case 1: return new ArrayList<Link>(Arrays.asList(new Link(6,14), new Link(3,9), new Link(2,7)));
                    case 2: return new ArrayList<Link>(Arrays.asList(new Link(1,7), new Link(3, 10), new Link(4,15)));
                    case 3: return new ArrayList<Link>(Arrays.asList(new Link(1,9), new Link(6, 2), new Link(4,11), new Link(2, 10)));
                    case 4: return new ArrayList<Link>(Arrays.asList(new Link(2,15), new Link(3, 11), new Link(6,5)));
                    case 5: return new ArrayList<Link>(Arrays.asList(new Link(6,9), new Link(4,6)));
                    case 6: return new ArrayList<Link>(Arrays.asList(new Link(1,14), new Link(3, 2), new Link(5,9)));
                }
                throw new IllegalArgumentException("No such node: " + node);
            }
        });
        return spf.findShortestPaths();
    }

    public static void main(String[] args) {
        DataSource ds;
        Connection connection = null;
        try {
            ds = new SingleConnectionDataSource("jdbc:mysql://localhost:3306/imdb", "com.mysql.jdbc.Driver", "root", "rootpw");
            connection = ds.getConnection();
            Database db = new Database(connection);
            BaconFinder bf = new BaconFinder(db);
            // Kurczewska, Izabela
            // Armadillo, The
            // 't Hart, Josine
            // Bacon, Kevin (I)
            Path p = bf.findLongestPath("'t Hart, Josine");
            if (p == null) {
                System.out.println("Path was null!");
                return;
            }
            PathFormatter pf = new PathFormatter(db);
            System.out.println("Path: " + pf.format(p));

//            System.out.println(bf.findLongestPaths(1));

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NodeLookupException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
