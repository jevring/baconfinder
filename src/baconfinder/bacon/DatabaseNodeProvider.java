package baconfinder.bacon;

import baconfinder.db.Database;
import baconfinder.path.Link;
import baconfinder.spf.NodeLookupException;
import baconfinder.spf.NodeProvider;

import java.util.List;

/**
 * Creator: captain
 * Created: 2011-04-14-13:42
 */
public class DatabaseNodeProvider implements NodeProvider<Link> {
    private final Database db;
    private boolean includeEpisodes = false;
    private boolean includeTv = false;
    private boolean includeVideo = false;
    private boolean includeVideoGames = false;
    private boolean includeHimselfCredit = false;
    private boolean includeArchiveFootage = false;
    private boolean includeArchiveSound = false;
    private boolean includeUnconfirmed = false;
    private boolean includeUncredited = false;
    private boolean includeCreditOnly = false;

    public DatabaseNodeProvider(Database db) {
        this.db = db;
    }

    @Override
    public List<Link> getConnections(Integer node) throws NodeLookupException {
        return db.getConnections(node, includeEpisodes, includeTv, includeVideo, includeVideoGames, includeHimselfCredit, includeArchiveFootage, includeArchiveSound, includeUnconfirmed, includeUncredited, includeCreditOnly);
    }

    public boolean isIncludeEpisodes() {
        return includeEpisodes;
    }

    public void setIncludeEpisodes(boolean includeEpisodes) {
        this.includeEpisodes = includeEpisodes;
    }

    public boolean isIncludeTv() {
        return includeTv;
    }

    public void setIncludeTv(boolean includeTv) {
        this.includeTv = includeTv;
    }

    public boolean isIncludeVideo() {
        return includeVideo;
    }

    public void setIncludeVideo(boolean includeVideo) {
        this.includeVideo = includeVideo;
    }

    public boolean isIncludeVideoGames() {
        return includeVideoGames;
    }

    public void setIncludeVideoGames(boolean includeVideoGames) {
        this.includeVideoGames = includeVideoGames;
    }

    public boolean isIncludeHimselfCredit() {
        return includeHimselfCredit;
    }

    public void setIncludeHimselfCredit(boolean includeHimselfCredit) {
        this.includeHimselfCredit = includeHimselfCredit;
    }

    public boolean isIncludeArchiveFootage() {
        return includeArchiveFootage;
    }

    public void setIncludeArchiveFootage(boolean includeArchiveFootage) {
        this.includeArchiveFootage = includeArchiveFootage;
    }

    public boolean isIncludeArchiveSound() {
        return includeArchiveSound;
    }

    public void setIncludeArchiveSound(boolean includeArchiveSound) {
        this.includeArchiveSound = includeArchiveSound;
    }

    public boolean isIncludeUnconfirmed() {
        return includeUnconfirmed;
    }

    public void setIncludeUnconfirmed(boolean includeUnconfirmed) {
        this.includeUnconfirmed = includeUnconfirmed;
    }

    public boolean isIncludeUncredited() {
        return includeUncredited;
    }

    public void setIncludeUncredited(boolean includeUncredited) {
        this.includeUncredited = includeUncredited;
    }

    public boolean isIncludeCreditOnly() {
        return includeCreditOnly;
    }

    public void setIncludeCreditOnly(boolean includeCreditOnly) {
        this.includeCreditOnly = includeCreditOnly;
    }
}
