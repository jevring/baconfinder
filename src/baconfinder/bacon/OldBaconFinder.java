package baconfinder.bacon;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class finds the actor or actress with the highest bacon number.
 * It does this by staring with Kevin Bacon and fanning out into every
 * actor/actress that has worked with him. In step n+1, it does the same
 * this for all actors in step n. This naive implementation will generate
 * a path, but it may not be the shortest. To ensure that a path is indeed
 * the shortest, we will keep track of and check a set of conditions along
 * the way. These conditions should also prevent us from creating loops.
 *
 * In a way, it is similar to the depth-first-search model checker from before.
 * The primary difference is that we want the path of states, and those paths
 * have to be the shortest paths.
 *
 * Creator: captain
 * Created: 2011-04-10-12:41
 */
public class OldBaconFinder {
    private final Map<String, Set<Integer>> actorToMovies = new HashMap<String, Set<Integer>>();
    private final Map<Integer, String> movies = new HashMap<Integer, String>();
    private final Map<String, Integer> movieKeys = new HashMap<String, Integer>();
    private final Pattern titlePattern = Pattern.compile("(.+)\\s\\((?:\\d|\\?){4}(?:/.+)?\\)(.*)");

    private final Pattern dataLine = Pattern.compile("(.*)\\t(.+)");

    private int currentKey = 0;

    private void findMoviesForActor(String actor) {
        System.out.println("Movies for '" + actor + "': ");
        if (actorToMovies.get(actor) != null) {
            for (Integer movieId : actorToMovies.get(actor)) {
                System.out.println(movieId + ": " + movies.get(movieId));
            }
        } else {
            System.out.println("No movies found");
        }
    }

    // todo: have a thing for including tv shows. if we do, we must use the whole episode name.
    // i.e.: '"Dinner: Impossible" (2007) {Mother's Day Madness (#5.5)}'
    // rather than just: 'Dinner: Impossible'


    // todo: actually, load() should probably still include the tv shows, but the search shouldn't.
    // we can do this when we put things into the database, but not when we are loading things into memory
    private void load(String file, boolean includeTv) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        BufferedReader in = new BufferedReader(new InputStreamReader(fis));
        try {
            String line;
/*
            int i = 0;
            Integer currentActorId = null;
            while ((line = in.readLine()) != null) {
                i++;
                if (line.trim().length() == 0) continue; // skip empty lines
                Matcher m = dataLine.matcher(line);
                if (m.matches()) {

                    String actor = m.group(1).trim();
                    Integer titleId = null;
                    Matcher titleMatcher = titlePattern.matcher(m.group(2));
                    if (titleMatcher.matches()) {
                        String title = m.group(1);
                        titleId = getTitleIdFromDatabase(title); // todo: remove the "from database" from everything later
                    }

                    if (!actor.isEmpty()) {
                        currentActorId = addActorToDatabase(actor);
                    }

                    connectActorToMovie(currentActorId, titleId);
                }
            }
*/

            int i = 0;
            Set<Integer> moviesForCurrentActor = null;
            while ((line = in.readLine()) != null) {
                i++;
                if (line.trim().length() == 0) continue; // skip empty lines
                Matcher m = dataLine.matcher(line);
                if (m.matches()) {

                    String actor = m.group(1).trim();
                    if (m.group(2).trim().startsWith("\"") && !includeTv) continue; // skip tv shows
                    Integer title;
                    try {
                        title = getTitleId(m.group(2).trim());

                    } catch (Exception e) {
                        System.out.println(e.getMessage() + ": '" + line + "'");
                        continue;
                    }

                    if (!actor.isEmpty()) {
                        moviesForCurrentActor = new HashSet<Integer>();
                        actorToMovies.put(actor, moviesForCurrentActor);
                    }

                    assert(moviesForCurrentActor != null);
                    moviesForCurrentActor.add(title);
                }
            }
        } finally {
            fis.close();
        }
    }

    private Integer addActorToDatabase(String actor) {
        // todo: this should not add something that already exists. It should get the id.


        return null;
    }

    private Integer getTitleId(String title) {
        Matcher m = titlePattern.matcher(title);
        if (m.matches()) {
            String actualTitle = m.group(1);
            Integer movieKey = movieKeys.get(actualTitle);
            if (movieKey == null) {
                currentKey++;
                movieKeys.put(actualTitle, currentKey);
                movies.put(currentKey, actualTitle);
                movieKey = currentKey;
//                System.out.println("Added title '" + actualTitle + "' with key " + movieKey);
            } else {
//                System.out.println("Found key for title '" + actualTitle + "': " + movieKey);
            }
            return movieKey;
        } else {
            throw new IllegalArgumentException("Title does not match pattern: '" + title + "'");
        }
    }

    // start by finding everyone who ever worked with bacon.
    // this should be doable without a database, just reading the thing in to memory

    public static void main(String[] args) {
        OldBaconFinder bf = new OldBaconFinder();
        try {
            //bf.load(args[0], false); // actors
            long s = System.currentTimeMillis();
            bf.load(args[1], false); // actresses
            System.out.println("Loading took " + (System.currentTimeMillis() - s) + " milliseconds");
            //bf.findMoviesForActor("Bacon, Kevin");

            bf.findMoviesForActor("Aalda, Mariann");
            bf.findMoviesForActor("Abate, Elizabeth (II)");
            bf.findMoviesForActor("Aberdeen Marcus, Bernie");
            bf.findMoviesForActor("Ackerman, Marie");
            bf.findMoviesForActor("Adams, Erin (I)");
            bf.findMoviesForActor("Adassa, Sylvie");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
