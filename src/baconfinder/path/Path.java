package baconfinder.path;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * target: actor.
 * using: movie.
 *
 * Creator: captain
 * Created: 2011-04-11-01:23
 */
public class Path implements Iterable<Link> {
    private final List<Link> links;
    private final int source;

    public Path(int source) {
        // todo: we may have to re-think having a source here, as we're going to need to have a leading path in the implementation anyway
        this(source, new LinkedList<Link>());
    }

    private Path(Integer source, List<Link> links) {
        this.source = source;
        this.links = links;
    }

    public Path newPathTo(Link link) {
        Path p = new Path(this.source, new LinkedList<Link>(this.links));
        p.addLink(link.target, link.using);
        return p;
    }

    /**
     * The first link to a path, which MUST be to the source itself, with link=-1, will be ignored.
     * @param node
     * @param link
     */
    public void addLink(int node, int link) {
        if (links.isEmpty() && node == source && link == -1) return; // don't bother with the self-referencing first dummy link
        links.add(new Link(node, link));
    }

    @Deprecated
    public boolean contains(int node) {
        if (node == source) {
            return true;
        }
        for (Link link : links) {
            if (link.target == node) {
                return true;
            }
        }
        return false;
    }

    public int length() {
        return links.size();
    }

    public Integer getSource() {
        return source;
    }

    @Override
    public Iterator<Link> iterator() {
        return links.iterator();
    }

    @Override
    public String toString() {
        return "(" + links.size() + "){" + source + "->" + links + "}";
    }
}
