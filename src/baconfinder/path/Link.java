package baconfinder.path;

/**
 * Creator: captain
 * Created: 2011-04-11-01:32
 */
public class Link {
    public final int target;
    public final int using;
    //public final int length = 1; // for this project, this is correct. If we want to re-use it in the future, it might change

    public Link(int target, int using) {
        this.target = target;
        this.using = using;
    }

    @Override
    public String toString() {
        return "with '" + target + "' in '" + using + "'";
    }
}
