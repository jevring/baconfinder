package baconfinder.path;

import baconfinder.db.Database;

import java.sql.SQLException;

/**
 * Creator: captain
 * Created: 2011-04-13-18:04
 */
public class PathFormatter {
    private final Database db;

    public PathFormatter(Database db) {
        this.db = db;
    }

    public String format(Path path) throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append(path.length()).append("Steps: '").append(db.getActorName(path.getSource())).append("' was in '");

        boolean firstLinkSkipped = false;
        for (Link link : path) {
            if (!firstLinkSkipped) {
                firstLinkSkipped = true;
                continue;
            }
            sb.append(db.getTitleName(link.using)).append("' with '").append(db.getActorName(link.target)).append("'");
            sb.append(" who was in '");
        }
        // todo: clean this up so we don't get the extra " who was in "...
        // in fact, we should do something more useful here.
        // perhaps create a new object that works a bit better
        return sb.toString();
    }
}
