package baconfinder.db;

import javax.sql.DataSource;
import java.io.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Creator: captain
 * Created: 2011-04-10-22:20
 */

public class Loader {
    // WORKING
    //private final Pattern movieTitlePattern = Pattern.compile("(.+)\\s\\((?:\\d|\\?){4}(?:/.+)?\\)(.*)");
    //private final Pattern tvEpisodePattern = Pattern.compile("\"(.+)\"\\s\\((?:\\d|\\?){4}(?:/.+)?\\)(?:\\s+\\{(.+)\\})?(?:\\s+\\[(.+)\\])?(.*?)");

    // title, year, TV/V*, comment*(up to 10. I hope that's enough, since it's arbitrary, and there's no list), billing*,billingnumber*
    // (voice[: XXX Version]) (credit only) (archive sound) ([also} archive footage) (as XX YY) (unconfirmed) (uncredited)
    private final Pattern movieTitlePattern = Pattern.compile("(.+)\\s\\(((?:\\d|\\?){4})(?:/.+)?\\)(?:\\s\\((V|TV|VG)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\[(.+)\\])?(?:\\s+\\<(.+)\\>)?");
    private final Pattern tvEpisodePattern = Pattern.compile("\"(.+)\"\\s\\(((?:\\d|\\?){4})(?:/.+)?\\)(?:\\s+\\{(.+)\\})?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\[(.+)\\])?(?:\\s+\\<(.+)\\>)?");
    private final Pattern dataLine = Pattern.compile("(.*)\\t(.+)");
    private final NumberFormat percentFormatter = DecimalFormat.getPercentInstance();
    private final DateFormat time = new SimpleDateFormat("HH:mm:ss");


    private final Database db;

    public Loader(Database db) {
        this.db = db;
    }

    private void load(String path) throws IOException, SQLException {

        // note: if we limit the heap space, we run in to problems where GC takes too long, so it gets slow.
        // I don't know what is making it slow, though, no data structure should grpw.


        File file = new File(path);
        System.out.println("Loading " + file);
        // note: caching the actors did nothing for the lines-per-second rating. it's almost silly..
        long start = System.currentTimeMillis();
        long linesRead = 0;
        long length = file.length();
        long bytesRead = 0;
        FileInputStream fis = new FileInputStream(file);
        BufferedReader in = new BufferedReader(new InputStreamReader(fis));
        try {
            String line;
            Integer currentActorId = null;
            while ((line = in.readLine()) != null) {
                linesRead++;
                if (linesRead % 10000 == 0) {
                    double elapsedSeconds = (double)(System.currentTimeMillis() - start) / 1000.0d;
                    double lineRate = (double)linesRead / elapsedSeconds;
                    double byteRate = (double)bytesRead / elapsedSeconds;
                    double completion = (double)bytesRead / (double)length;
                    double secondsLeft = (length - bytesRead) / byteRate;

                    Calendar c = Calendar.getInstance();
                    c.add(Calendar.SECOND, (int) secondsLeft);

                    System.out.println((int)lineRate + " Lines per second. " + percentFormatter.format(completion) + " complete. ETA: " + time.format(c.getTime()));
                }
                bytesRead+= line.length();
                if (line.trim().length() == 0) continue; // skip empty lines
                if (line.contains("{{SUSPENDED}}")) continue; // skip suspended movies
                Matcher dm = dataLine.matcher(line);
                if (dm.matches()) {

                    String actor = dm.group(1).trim();
                    if (!actor.isEmpty()) {
                        currentActorId = db.addOrReplaceActor(actor);
                    }

                    String title = dm.group(2).trim();

                    Integer titleId;
                    Matcher m;
                    // 1: title
                    // 2: year
                    // 3: TV|V|VG or episode
                    // 4-13: comments
                    // 14: billing
                    // 15: billing number

                    if (title.startsWith("\"")) {
                        m = tvEpisodePattern.matcher(title);
                    } else {
                        m = movieTitlePattern.matcher(title);
                    }
                    if (m.matches()) {
                        String name = m.group(1).trim();

                        int year = 0;
                        try {
                            year = Integer.parseInt(m.group(2));
                        } catch (NumberFormatException e) {
                            // this happens when the year is occasionally ????.
                            // while we could check for this, it happens so rarely that it's easier to simply handle it here
                            if (!"????".equals(m.group(2))) {
                                System.err.println("Unparsable year '" + m.group(2) + "' in line: " + line);
                            }
                        }

                        String typeOrEpisode = m.group(3);

                        boolean uncredited = false;
                        boolean unconfirmed = false;
                        boolean archiveFootage = false;
                        boolean alsoArchiveFootage = false;
                        boolean alsoArchiveSound = false;
                        boolean creditOnly = false;
                        boolean archiveSound = false;

                        for (int i = 4; i < 13; i++) {
                            String comment = m.group(i);
                            if ("uncredited".equals(comment)) {
                                uncredited = true;
                            } else if ("unconfirmed".equals(comment)) {
                                unconfirmed = true;
                            } else if ("archive footage".equals(comment)) {
                                archiveFootage = true;
                            } else if ("archive sound".equals(comment)) {
                                archiveSound = true;
                            } else if ("also archive footage".equals(comment)) {
                                alsoArchiveFootage = true;
                            } else if ("also archive sound".equals(comment)) {
                                alsoArchiveSound = true;
                            } else if ("credit only".equals(comment)) {
                                creditOnly = true;
                            }
                        }

                        boolean creditAsHimself = m.group(14) != null && ("Himself".equalsIgnoreCase(m.group(14)) || "Herself".equalsIgnoreCase(m.group(14)));

                        titleId = db.addOrReplaceTitle(name, year, typeOrEpisode, "TV".equalsIgnoreCase(typeOrEpisode), "V".equalsIgnoreCase(typeOrEpisode), "VG".equalsIgnoreCase(typeOrEpisode));
                        db.connectActorToTitle(currentActorId, titleId, uncredited, unconfirmed, archiveFootage, alsoArchiveFootage, creditOnly, archiveSound, alsoArchiveSound, creditAsHimself);
                    } else {
                        System.out.println("Title did not match pattern: '" + title + "'");
                    }
                }
            }
        } finally {
            fis.close();
        }
        long elapsedSeconds = (long)((double)(System.currentTimeMillis() - start) / 1000.0d);
        System.out.println("Loaded file: '" + file + "' in " + elapsedSeconds + " seconds");
    }

    public static void mainx(String[] args) {
        // title, year, TV/V*, comment*(up to 10. I hope that's enough, since it's arbitrary, and there's no list), billing*,billingnumber*
        // (voice[: XXX Version]) (credit only) (archive sound) ([also} archive footage) (as XX YY) (unconfirmed) (uncredited)
        Pattern movieTitlePattern = Pattern.compile("(.+)\\s\\(((?:\\d|\\?){4})(?:/.+)?\\)(?:\\s\\((V|TV|VG)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\[(.+)\\])?(?:\\s+\\<(.+)\\>)?");
        Pattern tvEpisodePattern = Pattern.compile("\"(.+)\"\\s\\(((?:\\d|\\?){4})(?:/.+)?\\)(?:\\s+\\{(.+)\\})?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\((.+?)\\))?(?:\\s+\\[(.+)\\])?(?:\\s+\\<(.+)\\>)?");
        String s = "Jekyll and Hyde (2010) {{SUSPENDED}}";
        Matcher m = movieTitlePattern.matcher(s);
        if (m.matches()) {
            for (int i = 1; i < m.groupCount(); i++) {
                System.out.println(i + ": " + m.group(i));
            }
        } else {
            System.out.println("No match");
        }
    }

    public static void main(String[] args) {
        load0(args[0]);
    }

    private static void load0(String path) {
        DataSource ds;
        Connection connection = null;
        try {
            ds = new SingleConnectionDataSource("jdbc:mysql://localhost:3306/imdb", "com.mysql.jdbc.Driver", "root", "rootpw");
            connection = ds.getConnection();
            Database db = new Database(connection);
            Loader loader = new Loader(db);

            loader.load(path);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
