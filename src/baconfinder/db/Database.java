package baconfinder.db;

import baconfinder.path.Link;
import baconfinder.spf.NodeLookupException;
import baconfinder.spf.NodeProvider;

import java.sql.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Creator: captain
 * Created: 2011-04-11-01:44
 */
public class Database {
    private final Connection connection;

    public Database(Connection connection) {
        this.connection = connection;
    }

    int addOrReplaceTitle(String title, int year, String episodeName, boolean tv, boolean video, boolean videogame) throws SQLException {
        PreparedStatement insert = null;
        PreparedStatement select = null;
        ResultSet rs = null;
        try {
            // http://bogdan.org.ua/2007/10/18/mysql-insert-if-not-exists-syntax.html
            // mysql specific, but I'm fine with that, for now
            // REPLACE INTO didn't work, because it generated new IDs, which was clearly wrong
            insert = connection.prepareStatement("INSERT IGNORE INTO imdb.titles (name, year, tv, video, videogame, episode_name) VALUES (?, ?, ?, ?, ?, ?)");
            insert.setString(1, title);
            insert.setInt(2, year);
            insert.setBoolean(3, tv);
            insert.setBoolean(4, video);
            insert.setBoolean(5, videogame);
            insert.setString(6, episodeName);
            insert.executeUpdate();

            if (episodeName == null) {
                // it's retarded that I can't check nullity by doing "x = null". I have to do "x is null". fuckers...
                select = connection.prepareStatement("SELECT id FROM imdb.titles WHERE name = ? AND year = ? AND tv = ? AND video = ? AND videogame = ? AND episode_name is null");
            } else {
                select = connection.prepareStatement("SELECT id FROM imdb.titles WHERE name = ? AND year = ? AND tv = ? AND video = ? AND videogame = ? AND episode_name = ?");
                select.setString(6, episodeName);
            }
            select.setString(1, title);
            select.setInt(2, year);
            select.setBoolean(3, tv);
            select.setBoolean(4, video);
            select.setBoolean(5, videogame);

            rs = select.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                throw new SQLException("Could not find added title: " + title);
            }
        } finally {
            safeClose(rs);
            safeClose(select);
            safeClose(insert);
        }
    }

    int addOrReplaceActor(String actor) throws SQLException {
        PreparedStatement insert = null;
        PreparedStatement select = null;
        ResultSet rs = null;
        try {
            // http://bogdan.org.ua/2007/10/18/mysql-insert-if-not-exists-syntax.html
            // mysql specific, but I'm fine with that, for now
            // REPLACE INTO didn't work, because it generated new IDs, which was clearly wrong
            insert = connection.prepareStatement("INSERT IGNORE INTO imdb.actors (name) VALUES (?)");
            insert.setString(1, actor);
            insert.executeUpdate();

            select = connection.prepareStatement("SELECT id FROM imdb.actors WHERE name = ?");
            select.setString(1, actor);
            rs = select.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                throw new SQLException("Could not find added actor: " + actor);
            }
        } finally {
            safeClose(rs);
            safeClose(select);
            safeClose(insert);
        }
    }

    void connectActorToTitle(Integer actorId, Integer titleId, boolean uncredited, boolean unconfirmed, boolean archiveFootage, boolean alsoArchiveFootage, boolean creditOnly, boolean archiveSound, boolean alsoArchiveSound, boolean creditAsHimself) throws SQLException {
        PreparedStatement preparedStatement = null;
        try {
            // http://bogdan.org.ua/2007/10/18/mysql-insert-if-not-exists-syntax.html
            // mysql specific, but I'm fine with that, for now
            preparedStatement = connection.prepareStatement("INSERT IGNORE INTO imdb.actors_to_titles VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            preparedStatement.setInt(1, actorId);
            preparedStatement.setInt(2, titleId);
            preparedStatement.setBoolean(3, uncredited);
            preparedStatement.setBoolean(4, unconfirmed);
            preparedStatement.setBoolean(5, archiveFootage);
            preparedStatement.setBoolean(6, alsoArchiveFootage);
            preparedStatement.setBoolean(7, creditOnly);
            preparedStatement.setBoolean(8, archiveSound);
            preparedStatement.setBoolean(9, alsoArchiveSound);
            preparedStatement.setBoolean(10, creditAsHimself);
            preparedStatement.executeUpdate();
        } finally {
            safeClose(preparedStatement);
        }
    }

    @Deprecated
    public Integer getActorId(String actor) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT id FROM imdb.actors WHERE name = ?");
            preparedStatement.setString(1, actor);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("id");
            } else {
                return null;
            }
        } finally {
            safeClose(preparedStatement);
            safeClose(resultSet);
        }
    }

    public String getActorName(Integer actor) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT name FROM imdb.actors WHERE id = ?");
            preparedStatement.setInt(1, actor);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("name");
            } else {
                return null;
            }
        } finally {
            safeClose(preparedStatement);
            safeClose(resultSet);
        }
    }

    @Deprecated
    public Integer getTitleId(String title, int year, String episodeName, boolean tv, boolean video, boolean videogame) throws SQLException {
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT id FROM imdb.titles WHERE name = ? AND year = ? AND episode_name = ?");

            preparedStatement.setString(1, title);
            preparedStatement.setInt(2, year);
            preparedStatement.setString(3, episodeName);
            preparedStatement.setBoolean(4, tv);
            preparedStatement.setBoolean(5, video);
            preparedStatement.setBoolean(6, videogame);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("id");
            } else {
                return null;
            }
        } finally {
            safeClose(preparedStatement);
            safeClose(resultSet);
        }
    }

    public String getTitleName(Integer title) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT name FROM imdb.titles WHERE id = ?");
            preparedStatement.setInt(1, title);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("name");
            } else {
                return null;
            }
        } finally {
            safeClose(preparedStatement);
            safeClose(resultSet);
        }
    }

    public List<Integer> getTitlesForActor(Integer actorId) throws SQLException {
        List<Integer> titlesForActor = new LinkedList<Integer>();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT title_id FROM imdb.actors_to_titles WHERE actor_id = ?");
            preparedStatement.setInt(1, actorId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                titlesForActor.add(resultSet.getInt("title_id"));
            }
        } finally {
            safeClose(preparedStatement);
            safeClose(resultSet);
        }
        System.out.println("Titles for actor (" + actorId + "): " + titlesForActor);
        return titlesForActor;
    }

    public List<Integer> getActorsForTitle(Integer titleId) throws SQLException {
        List<Integer> actorsForTitle = new LinkedList<Integer>();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT actor_id FROM imdb.actors_to_titles WHERE title_id = ?");
            preparedStatement.setInt(1, titleId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                actorsForTitle.add(resultSet.getInt("actor_id"));
            }
        } finally {
            safeClose(preparedStatement);
            safeClose(resultSet);
        }
        //System.out.println("Actors for title (" + titleId + "): " + actorsForTitle);
        return actorsForTitle;
    }

    @Deprecated
    public Map<String, Integer> getActorCache() throws SQLException {
        Map<String, Integer> actorCache = new HashMap<String, Integer>();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT id, name FROM imdb.actors");
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                actorCache.put(resultSet.getString("name"), resultSet.getInt("id"));
            }
        } finally {
            safeClose(preparedStatement);
            safeClose(resultSet);
        }
        return actorCache;

    }

    public List<Link> getConnections(int actorId, boolean includeEpisodes, boolean includeTv, boolean includeVideo, boolean includeVideoGames, boolean includeHimselfCredit, boolean includeArchiveFootage, boolean includeArchiveSound, boolean includeUnconfirmed, boolean includeUncredited, boolean includeCreditOnly) throws NodeLookupException {
        List<Link> connections = new LinkedList<Link>();
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            /*
            select * from actors_to_titles at2 where title_id in (select title_id from (
            select * from actors_to_titles at1
            where at1.actor_id = 510267) t)

            20 seconds (removing the inner select title_id from... pushes it up for 43 seconds. wtf?!
            (Remained at 20 seconds even after adding new indexes)

            "select at2.actor_id, at2.title_id from actors_to_titles at1\n" +
            "join actors_to_titles at2 on (at1.title_id = at2.title_id)\n" +
            "where at1.actor_id = ?"

            31 seconds (fell to 0 seconds after adding indexes on actor_id and title_id separately)
             */
            String statement =
                    "select at1.actor_id, at1.title_id\n" +
                    "from actors_to_titles at1\n" +
                    "join actors_to_titles at2 on (at1.title_id = at2.title_id)\n" +
                    "join titles t on (at2.title_id = t.id)\n" +
                    "where at2.actor_id = ?\n";
            // title properties
            if (!includeEpisodes) {
                statement += "and t.episode_name is null\n";
            }
            if (!includeTv) {
                statement += "and t.tv is false\n";
            }
            if (!includeVideo) {
                statement += "and t.video is false\n";
            }
            if (!includeVideoGames) {
                statement += "and t.videogame is false\n";
            }
            // link properties
            if (!includeHimselfCredit) {
                statement += "and at2.credit_as_himself is false\n";
            }
            if (!includeArchiveFootage) {
                statement += "and at2.archive_footage is false\n";
            }
            if (!includeArchiveSound) {
                statement += "and at2.archive_sound is false\n";
            }
            if (!includeUnconfirmed) {
                statement += "and at2.unconfirmed is false\n";
            }
            if (!includeUncredited) {
                statement += "and at2.uncredited is false\n";
            }
            if (!includeCreditOnly) {
                statement += "and at2.credit_only is false\n";
            }

            preparedStatement = connection.prepareStatement(statement);
            preparedStatement.setInt(1, actorId);
            long s = System.currentTimeMillis();
            resultSet = preparedStatement.executeQuery();
            //System.out.println("Getting connections took " + (System.currentTimeMillis() - s) + " milliseconds");
            while (resultSet.next()) {
                connections.add(new Link(resultSet.getInt("actor_id"), resultSet.getInt("title_id")));
            }
        } catch (SQLException e) {
            throw new NodeLookupException("Could not find connections to node: " + actorId, e);
        } finally {
            safeClose(preparedStatement);
            safeClose(resultSet);
        }
        //System.out.println("Links for actor (" + actorId + "): " + connections);
        return connections;
    }


    private void safeClose(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void safeClose(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
