package baconfinder.db;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Creator: captain
 * Created: 2011-04-10-22:24
 */
public class SingleConnectionDataSource implements DataSource {
    private PrintWriter logWriter;
    /**
     * In seconds.
     */
    private int loginTimeout;
    private final String connectionString;
    private final String driverClassName;
    private final String defaultUsername;
    private final String defaultPassword;
    private final Driver driver;
    private final Properties defaultProperties = new Properties();

    public SingleConnectionDataSource(String connectionString, String driverClassName, String defaultUsername, String defaultPassword) throws SQLException {
        this.connectionString = connectionString;
        this.driverClassName = driverClassName;
        this.defaultUsername = defaultUsername;
        this.defaultPassword = defaultPassword;

        try {
            Class.forName(driverClassName);
        } catch (ClassNotFoundException e) {
            throw new SQLException(e);
        }

        driver = DriverManager.getDriver(connectionString);

        defaultProperties.setProperty("user", defaultUsername);
        defaultProperties.setProperty("password", defaultPassword);
    }

    public Connection getConnection() throws SQLException {
        return driver.connect(connectionString, defaultProperties);
    }

    public Connection getConnection(String username, String password) throws SQLException {
        Properties p = new Properties(defaultProperties);
        p.setProperty("user", username);
        p.setProperty("password", password);
        return driver.connect(connectionString, p);
    }

    public PrintWriter getLogWriter() throws SQLException {
        return logWriter;
    }

    public void setLogWriter(PrintWriter out) throws SQLException {
        this.logWriter = out;
    }

    public void setLoginTimeout(int seconds) throws SQLException {
        this.loginTimeout = seconds;
    }

    public int getLoginTimeout() throws SQLException {
        return loginTimeout;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new SQLFeatureNotSupportedException("getParentLogger() not supported");
    }

    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }
}
