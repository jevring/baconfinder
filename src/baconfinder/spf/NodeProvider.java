package baconfinder.spf;

import java.util.List;

/**
 * Creator: captain
 * Created: 2011-04-13-17:46
 */
public interface NodeProvider<T> {

    public List<T> getConnections(Integer node) throws NodeLookupException;
}
