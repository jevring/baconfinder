package baconfinder.spf;

import baconfinder.path.Link;
import baconfinder.path.Path;

import java.util.*;


/**
 * Creator: captain
 * Created: 2011-04-13-16:25
 */
public class ShortestPathFinder {
    private final int source;
    private final NodeProvider<Link> nodeProvider;
    private final Map<Integer, Path> paths = new HashMap<Integer, Path>();
    private final Set<Integer> visitedNodes = new HashSet<Integer>();



    public ShortestPathFinder(int source, NodeProvider<Link> nodeProvider) {
        this.source = source;
        this.nodeProvider = nodeProvider;
    }

    public Map<Integer, Path> findShortestPaths() throws NodeLookupException {
        Set<Integer> enqueuedNodes = new HashSet<Integer>();
        Queue<Integer> queue = new LinkedList<Integer>();
        queue.add(source);

        paths.put(source, new Path(source));
        Integer currentNode;
        while ((currentNode = queue.poll()) != null) {
//            visitedNodes.add(currentNode.target);

            // _todo: the space problem is the queue.
            // FIXED using the check in enqueuedNodes!
            // after 1500 visited nodes, there are 3 MILLION items in the queue.
            // clearly, this is wrong.
            // checking visitedNodes before adding things to the queue doesn't help either.
            // checking the queue itself WOULD work, however it would take forever, since it is huge
            if (!visitedNodes.add(currentNode)) {
                // if we couldn't add it, that means that we've seen it before: skip.
                // This happens because we queue up a target multiple times because multiple
                // adjacent nodes might point to it.
                // The condition "A visited node will not be checked ever again; its distance recorded now is final and minimal"
                // indicates that we should do this, and it seems correct, too.
                // _todo: verify this by ensuring that all edges from this node has been checked the first time around.
                // Yes, it seems correct, because if both a previous node and this node have links
                // to a certain node, that's going to be added to the queue twice. Thus, we need to remove
                // it (or filter it, rather) any subsequent times it shows up
                System.out.println("Skipping queued link to visited node: " + currentNode);
                continue;
            }

            if (visitedNodes.size() % 500 == 0) {
                int combinedPathLength = 0;
                int longestPath = 0;
                int shortestPath = Integer.MAX_VALUE;
                for (Path path : paths.values()) {
                    combinedPathLength += path.length();
                    longestPath = Math.max(longestPath, path.length());
                    shortestPath = Math.min(shortestPath, path.length());
                }
                System.out.println("Visited nodes: " + visitedNodes.size() + ", Paths: " + paths.size() + ", Combined path length: " + combinedPathLength + ", Queue length: " + queue.size() + ", Enqueued objects: " + enqueuedNodes.size() + ", Longest path: " + longestPath + ", Shortest path: " + shortestPath);
            }


            // get the otherwise SHORTEST path (distance) to the current node.
            // in the first iteration, this will basically be a placeholder link with no "using".
            Path pathToCurrentNode = paths.get(currentNode);
            assert(pathToCurrentNode != null);
/*
            System.out.println("--------------------------");
            System.out.println("Looking at current node: " + currentNode + ", queue: " + queue);
            System.out.println("Current path: " + pathToCurrentNode);
            System.out.println("Visited nodes: " + visitedNodes);
            System.out.println("Unvisited nodes: " + getLinksToUnvisitedNodesFrom(currentNode));
*/
            for (Link link : getLinksToUnvisitedNodesFrom(currentNode)) {
                // This is breadth first, since we're adding to the end of a Queue.
                // To do a depth first, use a Stack instead
                if (!enqueuedNodes.contains(link.target)) {
                    // while this makes us keep each thing twice, it DRASTICALLY reduces
                    // both the size of the queue, and the time taken to look through it.
                    queue.add(link.target);
                    enqueuedNodes.add(link.target);
                }

                Path existingPathToNode = paths.get(link.target);
                if (existingPathToNode == null) {
                    existingPathToNode = pathToCurrentNode.newPathTo(link);
                    paths.put(link.target, existingPathToNode);
                    // if this is the first time we see the node, we know this is the shortest
                    // path to it, so we don't need to do any more checks
                    continue;
                }

                if (pathToCurrentNode.length() + 1 < existingPathToNode.length()) {
                    // if the distance to the node reachable from this node
                    // is SHORTER than the path we previously had to that
                    // node, this path becomes the new path to that node
                    Path previousPathToObject = paths.put(link.target, pathToCurrentNode.newPathTo(link));
                    System.out.println("Overwriting path to '" + link.target + "' (" + previousPathToObject + ") with shorter path " + pathToCurrentNode.newPathTo(link));

                }
            }
//            System.out.println("Paths: " + paths);
        }
        return paths;
    }

    private List<Link> getLinksToUnvisitedNodesFrom(Integer node) throws NodeLookupException {
        List<Link> connections = nodeProvider.getConnections(node);
        // this is essentially connections.removeAll(visitedNodes):
        Iterator<Link> iterator = connections.iterator();
        while (iterator.hasNext()) {
            Link next = iterator.next();
            if (visitedNodes.contains(next.target)) {
//                System.out.println("Removing " + next.target + ", since it exists in visitedNodes");
                iterator.remove();
            }
        }
        return connections;
    }
}
