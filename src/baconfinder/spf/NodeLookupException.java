package baconfinder.spf;

/**
 * Creator: captain
 * Created: 2011-04-13-18:50
 */
public class NodeLookupException extends Exception {
    public NodeLookupException(String message) {
        super(message);
    }

    public NodeLookupException(String message, Throwable cause) {
        super(message, cause);
    }

    public NodeLookupException(Throwable cause) {
        super(cause);
    }
}
