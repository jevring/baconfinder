# Bacon Finder
The idea of the bacon finder was the beat the [oracle of bacon](https://oracleofbacon.org/).
The app loads an imdb dump, which is freely available from imdb.com, into a database, then calculates the shortest path
between Kevin Bacon and everybody else. The goal is to find the longest shortest path, so to speak. Of all the shortest 
paths found, select the longest.